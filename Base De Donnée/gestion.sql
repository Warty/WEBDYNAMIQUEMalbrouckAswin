-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 29 Mai 2018 à 19:00
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `m2105`
--

-- --------------------------------------------------------

--
-- Structure de la table `gestion`
--

CREATE TABLE IF NOT EXISTS `gestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_materielle` char(25) DEFAULT NULL,
  `status` char(25) DEFAULT NULL,
  `nom_emp` char(25) DEFAULT NULL,
  `date_emp` date DEFAULT NULL,
  `type` char(25) DEFAULT NULL,
  `reservation` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gestion_type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Contenu de la table `gestion`
--

INSERT INTO `gestion` (`id`, `nom_materielle`, `status`, `nom_emp`, `date_emp`, `type`, `reservation`) VALUES
(2, 'switch', 'non dispo', 'ashwin', NULL, 'machine', 'y'),
(3, 'RaspberryPi', 'dispo', '', NULL, 'machine', 'n'),
(5, 'Hub', 'dispo', '', NULL, 'machine', 'n'),
(88, 'rasper', 'dispo', '', NULL, 'machine', 'n'),
(104, 'switch', 'dispo', '', NULL, 'machine', 'n'),
(108, 'windows', 'dispo', '', NULL, 'licence', 'n'),
(109, 'windows', 'dispo', NULL, NULL, 'licence', 'n'),
(110, 'mac', 'dispo', NULL, NULL, 'licence', 'n'),
(113, 'tournevis', 'dispo', '', NULL, 'objet', 'n'),
(116, 'coca', 'dispo', NULL, NULL, 'objet', 'n'),
(120, 'sosaz', 'dispo', NULL, NULL, 'objet', 'n'),
(121, 'souris', 'dispo', '', NULL, 'piece', 'n'),
(122, 'clavier', 'dispo', NULL, NULL, 'piece', 'n'),
(123, 'switch', 'dispo', NULL, NULL, 'machine', 'n'),
(124, 'pop', 'dispo', '', NULL, 'machine', 'n'),
(125, 'pop', 'dispo', '', NULL, 'machine', 'n'),
(126, 'switch', 'dispo', NULL, NULL, 'machine', 'n'),
(127, 'switch', 'dispo', NULL, NULL, 'machine', 'n'),
(128, 'switch', 'dispo', NULL, NULL, 'machine', 'n');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
