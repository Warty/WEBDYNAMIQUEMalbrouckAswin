<?php
try {
    $bdd = new PDO("mysql:host=localhost;dbname=m2105;", "root", "");
    echo "OK <br/>";
} catch (Exception $ex) {    //On capture les eventuelles exception pour les controller
    die('Erreur : ' . $ex->getMessage()); //On annule les message d'erreur
}
//Pour afficher dans le tableau
$reponse = $bdd->query('SELECT nom_materielle , type, id, status,nom_emp,reservation from gestion ');
$reponse1 = $bdd->query('SELECT * from notif ');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="Page1.css"/>  <!--liens vers la page css-->
        <title>Materiel</title>
    </head>

    <style>

        div#boite1
        {
            border: solid 0.5px aliceblue ;         /* DÃ©finit les paramÃ¨tre de la boÃ®te : solid : affichage en trait continu*/
            /*3px : Ã©paisseur du trait ; aqua : couleur des bordure de la boite      */
            padding-top:     0em;             /*Agrandit la boite vers le haut*/
            padding-bottom:  50em;            /*Agrandit la boite vers le bas */ 
            padding-left:  0em; 
            margin: auto;
            height: 80em;   /*720px*/  /*On annule l'utilisation des pading top et bottom pour avoir une largeur*hauteur plus prÃ©cise*/ 
            /*Remplissage de la boite nÂ°1*/
            background-image: url("contents/font_b1/b4.jpg");
            background-attachment: scroll;
            /*Fin du remplissage de la boite nÂ°1*/

            /*paramÃ¨tre de Remplissage de la boite nÂ°1*/
            background-repeat: no-repeat;
            background-position: center;
            /*Fin de paramÃ¨tre de Remplissage de la boite nÂ°1*/
        } 

        table 
        {
            border: medium solid #fb5255;
            border-collapse: collapse;
            width: 50%;
            height:2em;
        }
        th {
            font-family: Dosis light;
            font-size:1.6em;
            border: 2px solid aqua;
            width: 50%;

            background-color: #D0E3FA;

        }
        td 
        {
            font-family: Dosis light;
            font-size:2em;
            border: thin solid #6495ed;
            width: 50%;
            text-align: center;
            background-color: #ffffff;
        }
        div#admin
        {
            position: absolute;
            top: 25em;
            left:7em; /*ne deplace pas le tableau mais le  fais elargir cette fois ci*/
            /*background-color: #ffff00;*/
            /* margin-right: 75em;*/
            color:aliceblue;
            font-family: Dosis Light;
        }


        div#emprunt
        {

            position: absolute;
            top: 32em;
            left:93em; /*ne deplace pas le tableau mais le  fais elargir cette fois ci*/
            /*background-color: #ffff00;*/
            /* margin-right: 75em;*/
            color:aliceblue;
            font-family: Dosis Light;

        }
        div#outil
        {

            position:absolute;
            top: 7em;
            right:25em; /*ne deplace pas le tableau mais le  fais elargir cette fois ci*/
            width:25%;
            color:aliceblue;
            font-family: Dosis Light;

        }
        /********Message qui peuvent aparaitre lors des ajouts***********/
        div#ajout_error
        {
            position: absolute;
            top: 4em;
            left:42em; /*ne deplace pas le tableau mais le  fais elargir cette fois ci*/
            /*background-color: #ffff00;*/
            /* margin-right: 75em;*/
            font-size:36px;
            color:aliceblue;
            font-family: Dosis Light; 
        }
        div#ajout_error1
        {
            position: absolute;
            top: 3em;
            left:42em; /*ne deplace pas le tableau mais le  fais elargir cette fois ci*/
            /*background-color: #ffff00;*/
            /* margin-right: 75em;*/
            font-size:36px;
            color:aliceblue;
            font-family: Dosis Light; 
        }
        /********Message qui peuvent aparaitre lors des ajouts***********/
        div#horloge
        {

            position: absolute;
            top: 1.5em;
            right: 55em;
            /*background-color: #ffff00;*/
            /* margin-right: 75em;*/
            border: 2px solid aqua;
            padding:0.26em;
            font-family: Dosis light;
            font-size:36px;
            color: #fb5255;
            background-position: center;
        }
    </style>
    <body>

     
        
        <div id="boite1">
            <div id="admin">
                <h1>TABLEAU </h1>
                <table border="1">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Nom_materielle</th>
                            <th>type</th>
                            <th>Status</th>
                            <th>EMPRUNTER</th>
                            <th>RESERVATION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($reponse as $row): ?> <!--Effectue les mÃªme actions que le while dÃ©finit precedement-->
                            <tr>
                                <td><?= $row['id'] ?></td> 
                                <td><?= $row['nom_materielle'] ?></td>                       
                                <td><?= $row['type'] ?></td>
                                <td><?= $row['status'] ?></td>
                                <td><?= $row['nom_emp'] ?></td>  
                                <td><?= $row['reservation'] ?></td> 
                            </tr>
                        <?php endforeach; ?> 
                    </tbody>
                </table>
            </div>
            <!--////////////////////////////////////////////////////////////////////////////////-->
            <!--VISUALISATION DES EMPRUNTEUR-->
            <!--////////////////////////////////////////////////////////////////////////////////-->
            <div id="emprunt">
                <table border="1">
                    <thead>
                        <tr>
                            <th>ID  </th>
                            <th>Nom de l'emprunteur </th>
                            <th>Nom_materielle</th>
                            <th>DEBUT EMPRUNT</th>
                            <th>DATE DE RETOUR</th>
                            <th>VALIDATION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($reponse1 as $row): ?> <!--Effectue les mÃªme actions que le while dÃ©finit precedement-->
                            <tr>
                                <td><?= $row['id'] ?></td> 
                                <td><?= $row['nom_emp'] ?></td>    
                                <td><?= $row['nom_materielle'] ?></td>   
                                <td><?= $row['date_emp'] ?></td>
                                <td><?= $row['date_retour'] ?></td>
                                <td><?= $row['valid'] ?></td>
                            </tr>
                        <?php endforeach; ?>      
                    </tbody>
                </table>
            </div>
            <!--////////////////////////////////////////////////////////////////////////////////-->
            <!--            FIN          TABLEAU 
            <!--////////////////////////////////////////////////////////////////////////////////-->
            <!-------------------------------------------------------------->
            <!----------OUTIL ADMINISTRATEUR
            <!--------------------------------------------------------------> 
<?php
if (isset($_POST['choix_debut'])) {
    $choix = $_POST['choix_debut'];
    switch ($choix) 
    {

                case "add_one_user": {
                var_dump($_POST['choix_debut']);
                ?>

                <div id="outil">
                <table border="1" style="width:80%;height:19em;">
                <thead>
                <tr>
                    <th>NOUVELLE EMPRUNTEUR</th> 
                </tr>
                </thead>
                <tbody>
                <td>                     
                    <form action="admin.php" method="POST">         
                    UTILISATEUR A AJOUTER :<br>
                    <select name="login" id="nom_materielle" style="height:1.3em;font-size:1em;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5em;">

                <?php
                $reponse = $bdd->query('SELECT nom_emp  from notif where valid = \'en attente\'  ');  // good pour pas reserver
                while ($donnees = $reponse->fetch())
                { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                    echo '<option  value="' . $donnees['nom_emp'] . '">' . $donnees['nom_emp'] . '</option>';
                }
                $reponse->closeCursor();
                ?>
                    </select><br/>

             MATERIELLE A EMPRUNTER : 
            <select name="use" id="nom_materielle" style="height:1.3em;font-size:1em;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5em;">
            <?php
            $reponse = $bdd->query(' SELECT  nom_materielle from notif where  valid = \'en attente\'  ');  // good pour pas reserver
            while ($donnees = $reponse->fetch()) 
            { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                    echo '<option  value="' . $donnees['nom_materielle'] . '">' . $donnees['nom_materielle'] . '</option>';
            }
            $reponse->closeCursor();
            ?>
            </select>
            <br/>
            <input type ="submit"  value="V A L I D E R"  style="height:1.3em;font-size:36px;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5.2em;position:absolute;margin-left:-2em;margin-top:0.23em;">
            </form> 
            </td>
            </tbody>
            </table>
            </div>
            <?php
            }
            break;
            case "del_one_user": 
            {
            ?>
                <div id="outil">
                <table border="1" style="width:80%;height:19em;">
                <thead>
                    <tr>
                        <th>SUPPRIMER UTILISATEUR</th> 
                    </tr>
                    </thead>
                    <tbody>

                    <td>                     
                    <form action="admin.php" method="POST">         
                                UTILISATEUR A SUPPRIMER :<br>
                                <select name="delete_one" id="nom_materielle" style="height:1.3em;font-size:1em;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5em;">
                            <?php
                            $reponse = $bdd->query('SELECT nom_emp  from gestion where reservation = \'y\'  ');  // good pour pas reserver
                            while ($donnees = $reponse->fetch()) 
                            { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                                echo '<option  value="' . $donnees['nom_emp'] . '">' . $donnees['nom_emp'] . '</option>';
                            }
                            $reponse->closeCursor();
                            ?>
                            </select><br/>
                                MATERIELLE DE L'EMPRUNTER : 
                                <select name="delete_one_type" id="nom_materielle" style="height:1.3em;font-size:1em;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5em;">
                        <?php
                        $reponse = $bdd->query(' SELECT  nom_materielle from gestion where  reservation = \'y\' and status = \'non dispo\'    ');  // good pour pas reserver
                         while ($donnees = $reponse->fetch())
                         { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                        echo '<option  value="' . $donnees['nom_materielle'] . '">' . $donnees['nom_materielle'] . '</option>';
                        }
                        $reponse->closeCursor();
                        ?>
                        </select>
                        <br/>
                            <input type ="submit"  value="V A L I D E R"  style="height:1.3em;font-size:36px;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5.2em;position:absolute;margin-left:-2em;margin-top:0.23em;">
                    </form> 
                    </td>
                    </tbody>
                    </table>
                    </div><?php
                }
                break;

                case "add_one_mat": {
                          ?>
                        <div id="outil">
                            <table border="1" style="width:80%;height:19em;">
                                <thead>
                                    <tr>
                                        <th>AJOUT MATERIELLE</th> 
                                    </tr>
                                </thead>
                                <tbody>

                                <td>  
                                    <form action="admin.php" method="POST">

                                        MATERIELLE A AJOUTER : 
                                        <input type="text" name="add_mat1" style="height:1.3em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 9.5em;"> <br/>
                                        TYPE DE MATERIELLE : 
                                        <input type="text" name="add_mat_type1" style="height:1.3em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 9.5em;"> <br/>
                                        <input type ="submit"  value="V A L I D E R"  style="height:1.3em;font-size:36px;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5.2em;position:absolute;margin-left:-2em;margin-top:0.23em;">
                                    </form>  
                                </td>
                                </tbody>
                            </table>
                        </div><?php
                             }
                         break;
                        case "del_one_mat": 
                        {                        ?>
                            <div id="outil">
                                <table border="1" style="width:80%;height:19em;">
                                    <thead>
                                        <tr>
                                            <th>SUPPRIMER MATERIELLE</th> 
                                        </tr>
                                    </thead>
                                    <tbody>

                                    <td>                     
                    <form action="admin.php" method="POST">         
                                            MATERIELLE A SUPPRIMER :<br>
                <select name="del_mat" id="nom_materielle" style="height:1.3em;font-size:1em;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5em;">

                <?php
                $reponse = $bdd->query('SELECT nom_materielle from gestion where reservation = \'n\'  ');  // good pour pas reserver
                while ($donnees = $reponse->fetch()) 
                { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                    echo '<option  value="' . $donnees['nom_materielle'] . '">' . $donnees['nom_materielle'] . '</option>';
                }
                $reponse->closeCursor();
                ?>
                </select><br/>
                    <br/>
                    <input type ="submit"  value="V A L I D E R"  style="height:1.3em;font-size:36px;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5.2em;position:absolute;margin-left:-2em;margin-top:0.23em;">
                    </form> 
                    </td>
                    </tbody>
                    </table>
                    </div><?php
                }

                     break;
                }
        }
            ?>
            <!-------------------------------------------------------------->
            <!----------FORMULAIRE SUPPRESSION D'UN UTILISATEUR
            <!-------------------------------------------------------------->

            <!-------------------------------------------------------------->
            <!----------TRAITEMENT DES ACTIONS
            <!-------------------------------------------------------------->
<?php
    ////////////////////////////   
    //AJOUT D'UN UTILISATEUR 4551
    //////////////////////////// 
    if (isset($_POST["login"]) && isset($_POST["use"])) 
    {
        $name = $_POST['login'];
        $use = $_POST['use'];

        
        //Pour verifier que le materielle qu'entre l'administrateur existe bien cas ou deux onglet sont ouvert 
        $reponse = $bdd->query('SELECT nom_materielle  from gestion where nom_materielle ="' . $use . '" and  status = \'dispo\'  ');
        while ($donnees = $reponse->fetch()) 
                { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                    $x = $donnees['nom_materielle'];
                    //echo $x ;
                }
                $reponse->closeCursor();
        //Fin
    
    //Pour s'assurer que l'utilisateur  existe conformement au materielle qu'il souhaite emprunter
    $reponse = $bdd->query('SELECT nom_emp  from notif where nom_emp ="' . $name . '" and  nom_materielle ="' . $use . '"  and valid = \'en attente\'  ');
    while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
        $xa = $donnees['nom_emp'];
        //echo $xa;
    }
    $reponse->closeCursor();
    //FIN
    
    //Pour tracker la plus petite des id  corespondant aux materielle dispo afin d'eviter la redondance des noms dans le tableau
    $reponse = $bdd->query(' SELECT MIN(id) FROM `gestion` WHERE `nom_materielle`  ="' . $use . '" and  status = \'dispo\' ');
    while ($donnees = $reponse->fetch()) 
            { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                $rsave1 = $donnees['MIN(id)'];
                echo $rsave1;
            }
            $reponse->closeCursor();
        //FIN
        //ajout le 29-05-2018 pour éviter des validation redondans dans le tableau quand il sagit du meme materielle
        $reponse = $bdd->query(' SELECT MIN(id) FROM `notif` WHERE nom_emp ="' . $name . '" and  nom_materielle ="' . $use . '"  and valid = \'en attente\' ');
        while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
            $rsave2 = $donnees['MIN(id)'];
            echo $rsave2;
        }
        $reponse->closeCursor();
        //FIN
       
        if (!empty($x) && !empty($xa)) {       //materielle existant et nom emprunteur aussi
            echo ' Le materielle existe bien : TRAITEMENT DE LA REQUETE ';

        $req1 = $bdd->query
        ('
        UPDATE gestion SET nom_emp ="' . $name . '"  where nom_materielle ="' . $use . '" and id ="' . $rsave1 . '" and  status = \'dispo\'  
        ');
        $req2 = $bdd->query
        ('
        UPDATE gestion SET status = \'non dispo\'  where nom_materielle ="' . $use . '" and nom_emp ="' . $name . '" and id ="' . $rsave1 . '"
        ');
        $req3 = $bdd->query
        ('
        UPDATE gestion SET reservation= \'y\'  where nom_materielle ="' . $use . '" and nom_emp ="' . $name . '" and id ="' . $rsave1 . '"
        ');
        $req_add = $bdd->query
        ('
        UPDATE notif SET valid = \'VALIDER\'  where   nom_emp ="' . $name . '"  and nom_materielle ="' . $use . '" and id ="' . $rsave2 . '"
        ');

        header('Location: admin.php');
        } 
        else 
        {// SI  c'est  vide donc si le materielle n'existe pas
                        if (!empty($x) && empty($xa)) {  //materielle existant mais pas nom emprunteur aussi
                            ?><div id="ajout_error"><?php
                                echo ' Lutilisateur n\'existe pas' . '<br/>' . 'ou le materiel allouer' . '<br/>' . ' n\'est pas en accord' . '<br/>' . 'avec le souhait de l\'emprunteur ';
                        ?></div><?php
                    } else {
                        ?><div id="ajout_error1"><?php
                        // Si le materielle n'existe pas on demande ajouter un 
                        echo ' Le materielle que vous demander  n\'existe pas voulez vous en ajouter un ?' . '<br/>';
                        ?><!--Fermeture de php pour afficher du contenu html sous conditions-->
                            <form method="post" action="admin.php" name="formulaire">
                                <p>
                                    OUI:<input type="radio" name="oui" value="oui" style="height:0.79em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;font-weight:bold;width: 4em;margin-left:-1em">
                                    <br />
                                    NON:<input type="radio" name="non" value="non" style="height:0.79em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;font-weight:bold;width: 4em;margin-left:-1.3em">
                                    <br />
                                    <input type="submit"  value="V A L I D E R " style="height:1.3em;font-size:36px;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5.2em;position:absolute;margin-left:-0.23em;margin-top:0.23em;">
                                </p>
                            </form></div>
                        <?php
                        //Reprise du code normale PHP 
                    }
            }
            }//Fermeture de la premiere conditions if 
            else {
                echo ' VEUILLEZ REMPLIR LES CHAMPS SVP' . '<br/>';
            }
            ////////////////////////////   
            //FIN AJOUT
            ////////////////////////////
            ///////////////////////////////////////  
            //DEMANDE AJOUT SI MATERIELLE EXISTE PAS
            //////////////////////////////////////
            switch (isset($_POST)) {   // En fonction des choix de l'administrateur
                case isset($_POST['oui']): {
                        echo 'ok' . '<br/>';  // Pour tester
                        ?> <!--Fermeture pour intÃ©grer du HTML-->
                        <div id="ajout_error1">     
                            <form action="admin.php" method="POST">
                                <p style="margin-left:1em;margin-top:0.30em">
                                    SAISISEZ- LE -MATERIELLE- DONT -VOUS -VOULEZ -AJOUTER : 
                                </p>
                                <input type="text" name="add_mat" style="height:1.3em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 9.5em;"> <br/>
                                <p style="margin-left:1em;margin-top:-0.01em;margin-top:0.24em">
                                    SAISISEZ-SON-TYPE  : 
                                </p>
                                <input type="text" name="add_mat_type"style="height:1.3em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 9.5em;margin-top:-0.99em;position:absolute"> <br/>
                                <input type ="submit" value="V A L I D E R  " style="height:1.3em;font-size:36px;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 5.2em;position:absolute;margin-left:-0.23em;margin-top:0.24em;margin-left:1.299em;">
                            </form>  
                        </div>
                        <?php
                        // reprise du code php     
                    }
                    break;
                case isset($_POST['non']): {
                        echo 'nand';
                        header('Location: admin.php');
                    }
                    break;
            }
            //////////////////////////////////////// 
            //Traitement si le materielle n'existe pas
            // Inutile en raison des select qui liste que du materielle dispo 
            // mais tout de même utilise dans le cas ou l'adminitrateur ouvre deux onglet en même temps
            //////////////////////////////////////////
            if (isset($_POST['add_mat']) && isset($_POST['add_mat_type'])) 
            {
                echo ' tout fonctionne correctement' . '<br/>';

                //Conversion des variables afin de les intÃ©grer dans le insert
                $add_mat = $_POST['add_mat'];
               // $add_mat = mysql_real_escape_string($add_mat);
                $add_mat_type = $_POST['add_mat_type'];
                //$add_mat_type = mysql_real_escape_string($add_mat_type);
                ///Pour verifier que le type que saisi l'ADMIN existe bien
                $reponse = $bdd->query('SELECT type  from gestion where type ="' . $add_mat_type . '"  ');
                while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 


                    $x2 = $donnees['type'];
                    $x2; // contient les Ã©lÃ©ment de la requete et le nombre 
                }
                $reponse->closeCursor();
                if (!empty($x2)) {       //SI C pas est vide 
                    echo ' Le type de materielle existe bien : TRAITEMENT DE LA REQUETE ';
                    $req3 = $bdd->exec
                    ("                                                                                                     
                         INSERT INTO `m2105`.`gestion` (`id`, `nom_materielle`, `status`, `nom_emp`, `date_emp`, `type`, `reservation`) VALUES (NULL, '$add_mat', 'dispo', NULL, NULL, '$add_mat_type', 'n')
                    ");
                    header('Location: admin.php'); //reactualise la page pour voir touts de suite les modification  
                } else {
                    ?><div id="ajout_error1"><?php
                    echo 'Vous navez pas saisi correctement le type de materielle' . '<br/>';
                    ?></div><?php
                }
            } 
            else {

                echo '';
            } 
            ////////////////////////////   
            //FIN
            //////////////////////////// 
            ////////////////////////////   
            //Suppresion d'un utilisateur 5551
            //Même s'il s'agit d'un formulaire dynamique à deux champs le deuxieme champs n'est pas dépendante du premier champs <<expliquer
            /*dans le journal de bord>> de ce fais il y'a un risque d'erreur de selection par l'admin dont la neccesiter
            /*de garder la variable $x1 */
            //////////////////////////// 
            if (isset($_POST['delete_one']) && isset($_POST['delete_one_type'])) {
                $name_one = $_POST['delete_one']; //nom utilisateur
                $name_type = $_POST['delete_one_type']; // le materielle Ã  supprimer 
                //Pour selectionner l'utilisateur et le type de materielle qu'il a emprunter 
                $reponse1 = $bdd->query('SELECT nom_emp  from gestion where nom_emp ="' . $name_one . '" and nom_materielle ="' . $name_type . '"');//verifie que l'utilisateur à supprimer et concorde au materielle

                while ($donnees1 = $reponse1->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 

                    $x1 = $donnees1['nom_emp'];
                    echo $x1; //Contient l'utilisateur et le type de materielle qu'il a emprunter 
                }
                $reponse->closeCursor();

                //Pour éviter de supprimer tous les utilisateur portant le meme noms    
                $reponse = $bdd->query(' SELECT MIN(id) FROM `gestion` WHERE   nom_emp ="' . $name_one . '"  and `nom_materielle`  ="' . $name_type . '" ');
                while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                    $rsave1 = $donnees['MIN(id)'];
                    //echo $rsave1;
                    //var_dump($rsave1);
                }
                $reponse->closeCursor();


                if (!empty($x1) && !empty($rsave1)) {       //SI C pas est vide 

                    //L'ANNCIEN CODE NE SUPPRIMAIS PAS DANS LA TABLE NOTIF :
                    $reponse = $bdd->query(' SELECT MIN(id) FROM `notif` WHERE nom_emp = "' . $name_one . '" and `nom_materielle`  ="' . $name_type . '" and valid = \'VALIDER\'  ');
                    while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 
                        $rsave2 = $donnees['MIN(id)'];
                        //var_dump($rsave2);
                    }
                    $reponse->closeCursor();
                    //FIN
                    $req5 = $bdd->query
                        ('                                                                                                     
                            DELETE FROM notif WHERE nom_emp = "' . $name_one . '"  and id ="' . $rsave2 . '" and nom_materielle  ="' . $name_type . '" 
                        ');
                    echo ' Lemprunteur existe bien: TRAITEMENT DE LA REQUETE ';
                    //Traitement en deux temps pour passer de non dispo  Ã  dispo 
                    $req2 = $bdd->query('UPDATE gestion SET status = \'dispo\'  where nom_emp = "' . $name_one . '" and id = "' . $rsave1 . '"  ');
                    $req3 = $bdd->query('UPDATE gestion SET reservation = \'n\'  where nom_emp = "' . $name_one . '"  and id = "' . $rsave1 . '"');
                    $req1 = $bdd->query('UPDATE gestion SET nom_emp = \'\'  where nom_emp = "' . $name_one . '" and id = "' . $rsave1 . '" ');
                    header('Location: admin.php'); //reactualise la page pour voir touts de suite les modification
                } 
                else
                {
                    ?><div id="ajout_error"><?php
                    echo 'L\'emprunteur que vous avez selectionner n\'existe pas veuillez recommencer  ! ' . '<br/>';
                    ?></div><?php
                }
            }

            ////////////////////////////   
            //Ajout de materielle 455
            //////////////////////////// 
            if (isset($_POST['add_mat1']) && isset($_POST['add_mat_type1'])) {
                echo ' tout fonctionne correctement' . '<br/>';

                //Conversion des variables afin de les intÃ©grer dans le insert
                $add_mat1 = $_POST['add_mat1'];
                //$add_mat1 = mysql_real_escape_string($add_mat1);

                $add_mat_type1 = $_POST['add_mat_type1'];
                //$add_mat_type1 = mysql_real_escape_string($add_mat_type1);

                ///Pour verifier que le type que saisi l'ADMIN existe bien
                $reponse = $bdd->query('SELECT type  from gestion where type ="' . $add_mat_type1 . '"  ');
                while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 


                    $x3 = $donnees['type'];
                    $x3; // contient les Ã©lÃ©ment de la requete et le nombre 
                }
                $reponse->closeCursor();
                if (!empty($x3)) 
                {       //SI C pas est vide 
                    echo ' Le type de materielle existe bien : TRAITEMENT DE LA REQUETE ';
                    $req4 = $bdd->query
                            ("                                                                                                     
                                INSERT INTO `m2105`.`gestion` (`id`, `nom_materielle`, `status`, `nom_emp`, `date_emp`, `type`, `reservation`) VALUES (NULL, '$add_mat1', 'dispo', NULL, NULL, '$add_mat_type1', 'n')
                                ");
                                header('Location: admin.php'); //reactualise la page pour voir touts de suite les modification  
                }
                else 
                {
                    ?><div id="ajout_error"><?php
                    echo 'Vous navez pas saisi correctement le type de materielle' . '<br/>';
                    ?></div><?php
                }
            } 
            else 
            {

                echo '';
            }
            ////////////////////////////   
            //Fin
            //////////////////////////// 
            ////////////////////////////   
            //Suppresion de materielle 555
            //////////////////////////// 
            if (isset($_POST['del_mat'])) 
            {
                //echo ' tout fonctionne correctement' . '<br/>';
                //Conversion des variables afin de les intégrer  dans le insert
                $del_mat = $_POST['del_mat'];

                ///Pour verifier que le materielle que saisi l'ADMIN existe bien et qu'il est dispo sinon sa veut dire qu'il est emprunter
                $reponse = $bdd->query('SELECT nom_materielle  from gestion where nom_materielle ="' . $del_mat . '" and  status = \'dispo\' and reservation = \'n\' ');
                while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrer et organise dans l'array donÃ©es--> 


                    $x4 = $donnees['nom_materielle'];
                    $x4; // contient les élements de la requete
                    var_dump($x4);
                }
                $reponse->closeCursor();

                //Ligne inutilise 
                ///Pour avoir une precision des erreurs  imaginons qu'on supprime un materielle non dispo
                $reponse = $bdd->query('SELECT nom_materielle  from gestion where nom_materielle ="' . $del_mat . '" and  status = \'non dispo\'  ');
                while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrer et l'organise dans l'arrray donées--> 


                    $x5 = $donnees['nom_materielle'];
                    $x5; // contient les élement de la requête
                    var_dump($x5);
                }
                $reponse->closeCursor();
                //Fin

                //Pour ne pas tout supprimer d'un coup    
                $reponse = $bdd->query(' SELECT MIN(id) FROM `gestion` WHERE `nom_materielle`  ="' . $del_mat . '" and  status = \'dispo\' ');
                while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donnes--> 
                    $rsave1 = $donnees['MIN(id)'];
                    echo $rsave1;
                }
                $reponse->closeCursor();
                //FIN

                if (!empty($x4)) //x4 si l'admin ne se trompe pas et supprime un materielle bien dispo
                {       //SI C est pas  vide 
                    //echo ' Le type de materielle  Ã  supprimer existe bien : TRAITEMENT DE LA REQUETE ' . '<br/>';
                    $req5 = $bdd->query
                    ('                                                                                                     
                        DELETE FROM gestion WHERE nom_materielle  ="' . $del_mat . '" and id  ="' . $rsave1 . '"
                    ');
                    header ('Location: admin.php'); //reactualise la page pour voir touts de suite les modification 
                } 
                else 
                {       //Sachant qu'il s'agit ici d'un formulaire dynamique (ajouter depuis le 01-05-2018)  de suppresion  a un champs
                         // et qu'il liste que les materielle dispo il y a donc pas de chance de se tromper
                        // de ce fait ces codes contenu dans le else sont inutiles : 
                         //   if (!empty($x5)) 
                      //  {
                             //   ?><div id="ajout_error"><?php
                              //  echo 'Le materielle que vous voulez  supprimer a déjà eter emprunter' . '<br/>';
                                ?> </div> <?php
                      //  } 
                      //  else 
                       // {           
                       //     ?><div id="ajout_error"><?php
                        //    echo ' Le materielle que vous voulez supprimer n\'existe pas ' . '<br/>';
                          //      ?> </div> <?php
                       // }
                }
            } 
            else 
            {

                echo '';
            }
            ////////////////////////////   
            //Fin
            //////////////////////////// 
            ////////////////////////////   
            //Gestion DATE
            //////////////////////////// 
            $date = new DateTime(); // Contient la date d'aujourd'hui 
            //echo $date->format( 'Y/m/d');  
            //Pour compter le nombre de date correspondant a la date d'aujourd'hui, pour savoir le nombre de reservation a faire
            $reponse = $bdd->query(' SELECT COUNT(*) date_emp from notif where date_emp ="' . $date->format('Y/m/d') . '" and valid = \'en attente\'  ');

            while ($donnees = $reponse->fetch()) { // <!--Boucle qui parcour chaque entrÃ© et organise dans l'array donÃ©es--> 


                $xdat1 = $donnees['date_emp'];

                //echo $xdat1; affiche ici le nombre de date correspondant Ã  la date d'aujourd'hui
            }
            $reponse->closeCursor();
            //Fin
            //Div horloge qui se presente comme un menu deroulant
            ?><div id="horloge"><?php
            echo 'B O N J O U R ' . '<br/>';
            echo 'Il y\'a ' . $xdat1 . '  reservation  a  effetuer aujourd\'hui' . '<br/>';
            echo 'QUE VOULEZ VOUS FAIRE ?';
            ?><form action="admin.php" method="POST">
            <p>
                <select name="choix_debut" id="t" style="height:1.3em;font-size:1em;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 12em;">
                            <option   value="add_one_user" name ="add_one_user">Ajouter un  Emprunteur </option>
                            <option   value="del_one_user" name ="del_one_user">Supprimer un  Emprunteur </option>
                            <option   value="add_one_mat" name ="add_one_mat">Ajouter un Materielle</option>
                            <option   value="del_one_mat" name ="del_one_mat">Suprimer un materielle</option>
                </select>
            </p>
                            <input type ="submit" value="V A L I D E R " style="height:1.3em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 9.5em;">
                            <input type ="submit" value="off" name="off" style="height:1.3em;font-size:36px;margin-left:1.5em;font-family: Dosis light;background-color: #fb5255;color:aliceblue;width: 2em;">
              </form>
            </div>
            <?php
            if (isset($_POST['off'])) {
                header('Location: admin.php');
            }
            ?>
        </div>
    </body>
</html>